import mongoose from 'mongoose';

const ActionSchema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Employee',
      required: true,
    },
    label: {
      type: String,
      enum: ['create', 'read', 'update', 'delete'],
      required: true,
    },
    entity: {
      type: String,
      enum: [
        'account',
        'product',
        'order',
        'category',
        'promotion',
        'voucher',
        'message',
      ],
      required: true,
    },
    entityId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },
    description: {
      type: String,
      required: false,
    },
    metadata: {
      type: Map,
      of: String,
      required: false,
    },
    date: {
      type: Date,
      default: Date.now,
    },
  },
  { timestamps: true, versionKey: false },
);

const ActionModel = mongoose.model('Action', ActionSchema);
export default ActionModel;
