import mongoose from 'mongoose';

const EmployeeActionSchema = new mongoose.Schema(
  {
    employeeId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Employee',
      required: true,
    },
    actionType: {
      type: String,
      enum: ['promotion', 'demotion', 'leave', 'other'],
      required: true,
    },
    description: {
      type: String,
      required: false,
    },
    date: {
      type: Date,
      default: Date.now,
    },
  },
  { timestamps: true, versionKey: false },
);

const EmployeeActionModel = mongoose.model(
  'EmployeeAction',
  EmployeeActionSchema,
);
export default EmployeeActionModel;
