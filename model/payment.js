import mongoose from 'mongoose';

const paymentSchema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  paymentMethod: {
    type: String,
    enum: [
      'credit_card',
      'debit_card',
      'paypal',
      'bank_transfer',
      'cash_on_delivery',
    ],
    required: true,
  },
  amount: {
    type: Number,
    min: 0,
  },
  transactionCode: {
    type: String,
    unique: true,
  },
  paymentDate: {
    type: Date,
  },
  paymentStatus: {
    type: String,
    enum: ['pending', 'completed', 'failed'],
    default: 'pending',
  },
});
const Payment = mongoose.model('Payment', paymentSchema);

export default Payment;
