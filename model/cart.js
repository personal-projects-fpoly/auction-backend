import mongoose from 'mongoose';

const CartSchema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    },
    products: [
      {
        productId: {
          type: mongoose.Schema.Types.ObjectId,
          ref: 'Product',
          required: true,
        },
        price: {
          type: mongoose.Schema.Types.Decimal128,
          required: true,
        },
        quantity: {
          type: Number,
          default: 1,
        },
        totalPrice: {
          type: mongoose.Schema.Types.Decimal128,
          required: true,
        },
      },
    ],
  },
  { timestamps: true },
);

export const CartModel = mongoose.model('Cart', CartSchema);
