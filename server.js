import express from 'express';
import mongoose from 'mongoose';
import { Route } from './router/index.js';
import dotenv from 'dotenv';
import cors from 'cors';
import http from 'http';
import { Server } from 'socket.io';
import auction from './service/auction.js';
import cookieParser from 'cookie-parser';

dotenv.config();
auction.start();
const app = express();
app.use(
  cors({
    origin: 'http://localhost:4200',
    credentials: true,
  }),
);

const port = process.env.PORT || 3000;
const db = process.env.DB_URL;

const connectDB = async (url) => {
  try {
    await mongoose.connect(url);
    console.log('Connected to the database');
  } catch (error) {
    console.error('Database connection error:', error);
  }
};

connectDB(db);

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
Route(app);

const server = http.createServer(app);
export const io = new Server(server, {
  connectionStateRecovery: {},
  cors: {
    origin: '*',
    methods: ['GET', 'POST'],
    credentials: true,
  },
});

io.on('connection', (socket) => {
  console.log('Client connected');

  socket.on('disconnect', () => {
    console.log('Client disconnected');
  });
});

server.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
