import bcrypt from 'bcrypt';
import { UserModel } from '../model/user.js';
import generateTokenAndSetCookie from '../utils/generateToken.js';

export const AuthController = {
  signin: async (req, res) => {
    try {
      const { username, password } = req.body;
      if (!username || !password) {
        return res.status(400).json({ message: 'Missing required fields' });
      }
      const user = await UserModel.findOne({
        $or: [{ username: username }, { email: username }],
      });
      if (!user) {
        return res.status(404).json({ message: 'Username or email not found' });
      }
      const isMatch = await bcrypt.compare(password, user.password);
      if (!isMatch) {
        return res.status(401).json({ message: 'Wrong password' });
      }
      generateTokenAndSetCookie(user._id, res);

      return res.json({
        message: 'Signin success',
        data: {
          _id: user._id,
          name: user.name,
          username: user.username,
          email: user.email,
          role: user.role,
          balance: user.balance,
        },
      });
    } catch (error) {
      res
        .status(500)
        .json({ message: 'Internal server error', error: error.message });
    }
  },
  signinByToken: async (req, res) => {
    try {
      const user = req.user;
      generateTokenAndSetCookie(user._id, res);
      return res.json({
        message: 'Token đã được làm mới',
        data: {
          _id: user._id,
          name: user.name,
          username: user.username,
          email: user.email,
          role: user.role,
          balance: user.balance,
        },
      });
    } catch (error) {
      res
        .status(500)
        .json({ message: 'Internal server error', error: error.message });
    }
  },
  signup: async (req, res) => {
    try {
      const { name, username, email, password } = req.body;
      const existingUser = await UserModel.findOne({
        $or: [{ username: username }, { email: email }],
      });
      if (existingUser) {
        return res
          .status(400)
          .json({ message: 'Username or email already exists' });
      }
      const hashedPassword = await bcrypt.hash(password, 10);
      const newUser = new UserModel({
        name: name,
        username: username,
        email: email,
        password: hashedPassword,
        role: 'user',
      });
      await newUser.save();
      return res.status(201).json({ message: 'User registered successfully' });
    } catch (error) {
      res
        .status(500)
        .json({ message: 'Internal server error', error: error.message });
    }
  },
  signout: (req, res) => {
    try {
      res.cookie('jwt', '', {
        maxAge: 0,
        httpOnly: true,
        sameSite: 'strict',
        secure: process.env.NODE_ENV == 'production',
      });
      return res.status(200).json({ message: 'Logged out successfully' });
    } catch (error) {
      console.log('Error in logout controller', error.message);
      return res.status(500).json({ error: 'Internal Server Error' });
    }
  },
};
