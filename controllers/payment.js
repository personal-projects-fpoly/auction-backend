import Payment from '../model/payment.js';
import paymentApiCall from '../service/payment.js';
import { io } from '../server.js';
import generateQrCode from '../service/qrcode.js';

export const PaymentController = {
  createPayment: async (req, res) => {
    try {
      const { userId, paymentMethod, addInfo, startTime, amount } = req.body;
      if (!userId || !paymentMethod || !addInfo || !startTime || !amount) {
        return res.status(400).json({
          success: false,
          message:
            'Missing required fields: userId, paymentMethod, addInfo, startTime, amount',
        });
      }

      console.log(userId, paymentMethod, addInfo, startTime);
      const checkPayment = await paymentApiCall({
        addInfo,
        startTime,
      });

      let payment;

      if (checkPayment) {
        payment = new Payment({
          userId,
          paymentMethod,
          amount: checkPayment.value + 1,
          addInfo,
          paymentStatus: 'completed',
          transactionCode: checkPayment.transactionCode,
          paymentDate: new Date(),
        });
      } else {
        payment = new Payment({
          userId,
          paymentMethod,
          addInfo,
          paymentStatus: 'pending',
        });
      }

      await payment.save();

      io.emit(String(addInfo), {
        payment,
        message: checkPayment
          ? 'Thanh toán thành công!'
          : 'Thanh toán thất bại, vui lòng thực hiện lại!',
      });

      return res.status(201).json({
        success: true,
        message: checkPayment
          ? 'Payment created successfully'
          : 'Payment pending, please try again!',
        data: payment,
      });
    } catch (error) {
      console.error('Error creating payment:', error);
      return res.status(500).json({
        success: false,
        message: 'Error creating payment',
        error: error.message,
      });
    }
  },
  createQrCode: async (req, res) => {
    try {
      const { amount, addInfo } = req.body;
      if (!amount || !addInfo) {
        return res.status(400).json({
          success: false,
          message: 'Missing required fields: amount, addInfo',
        });
      }
      const qrCode = await generateQrCode(amount, addInfo);
      return res.status(200).json({
        success: true,
        message: 'Create QRCode successfully',
        data: qrCode,
      });
    } catch (error) {
      console.error('Error creating QRCode:', error);
    }
  },
  getPayments: async (req, res) => {
    try {
      const payments = await Payment.find();
      res.status(200).json({
        success: true,
        data: payments,
      });
    } catch (error) {
      res.status(400).json({
        success: false,
        message: 'Error fetching payments',
        error: error.message,
      });
    }
  },

  getPaymentById: async (req, res) => {
    try {
      const payment = await Payment.findById(req.params.id);
      if (!payment) {
        return res.status(404).json({
          success: false,
          message: 'Payment not found',
        });
      }
      res.status(200).json({
        success: true,
        data: payment,
      });
    } catch (error) {
      res.status(400).json({
        success: false,
        message: 'Error fetching payment',
        error: error.message,
      });
    }
  },
  updatePayment: async (req, res) => {
    try {
      const payment = await Payment.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
      });
      if (!payment) {
        return res.status(404).json({
          success: false,
          message: 'Payment not found',
        });
      }
      res.status(200).json({
        success: true,
        message: 'Payment updated successfully',
        data: payment,
      });
    } catch (error) {
      res.status(400).json({
        success: false,
        message: 'Error updating payment',
        error: error.message,
      });
    }
  },
  deletePayment: async (req, res) => {
    try {
      const payment = await Payment.findByIdAndDelete(req.params.id);
      if (!payment) {
        return res.status(404).json({
          success: false,
          message: 'Payment not found',
        });
      }
      res.status(200).json({
        success: true,
        message: 'Payment deleted successfully',
      });
    } catch (error) {
      res.status(400).json({
        success: false,
        message: 'Error deleting payment',
        error: error.message,
      });
    }
  },
};
