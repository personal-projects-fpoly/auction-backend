import { CartModel } from '../model/cart.js';
import { ProductModel } from '../model/product.js';

export const CartController = {
  getAll: async (req, res) => {
    try {
      const cart = await CartModel.find();
      if (!cart) {
        return res.status(404).json({ message: 'Cart not found.' });
      }
      res.status(200).json(cart);
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  },
  getByUser: async (req, res) => {
    const { userId } = req.params;

    try {
      const cart = await CartModel.findOne({ userId }).populate(
        'products.productId',
      );
      if (!cart) {
        return res.status(404).json({ message: 'Cart not found.' });
      }
      res.status(200).json(cart);
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  },

  updateCartItem: async (req, res) => {
    const { userId, productId } = req.params;
    const { quantity } = req.body;

    try {
      const cart = await CartModel.findOne({ userId });
      if (!cart) {
        return res.status(404).json({ message: 'Cart not found.' });
      }

      const cartItem = cart.products.find((item) =>
        item.productId.equals(productId),
      );
      if (!cartItem) {
        return res.status(404).json({ message: 'Cart item not found.' });
      }

      const product = await ProductModel.findById(productId);
      if (!product) {
        return res.status(404).json({ message: 'Product not found.' });
      }

      cartItem.quantity = quantity;
      cartItem.totalPrice = (
        cartItem.quantity *
        (parseFloat(product.price) - parseFloat(cartItem.discount || 0))
      ).toFixed(2);

      await cart.save();
      res.status(200).json({ message: 'Cart item updated successfully!' });
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  },

  removeFromCart: async (req, res) => {
    const { userId, productId } = req.params;

    try {
      const cart = await CartModel.findOne({ userId });
      if (!cart) {
        return res.status(404).json({ message: 'Cart not found.' });
      }

      const productIndex = cart.products.findIndex((item) =>
        item.productId.equals(productId),
      );
      if (productIndex === -1) {
        return res.status(404).json({ message: 'Cart item not found.' });
      }

      cart.products.splice(productIndex, 1);
      await cart.save();
      res.status(200).json({ message: 'Cart item removed successfully!' });
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  },

  clearCart: async (req, res) => {
    const { userId } = req.params;

    try {
      const result = await CartModel.updateOne(
        { userId },
        { $set: { products: [] } },
      );
      if (result.matchedCount === 0) {
        return res.status(404).json({ message: 'Cart not found.' });
      }
      res.status(200).json({ message: 'Cart cleared successfully!' });
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  },
};
