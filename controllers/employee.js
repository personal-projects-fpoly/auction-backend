// src/controllers/employeeActionController.js
import { StatusCodes } from 'http-status-codes';
import EmployeeActionModel from '../models/employeeAction.js';
import { employeeActionSchema } from '../validations/employeeAction.js';

const EmployeeActionController = {
  getAll: async (req, res) => {
    try {
      const employeeActions = await EmployeeActionModel.find();
      return res.status(StatusCodes.OK).json({
        message: 'Lấy tất cả hành động của nhân viên thành công.',
        data: employeeActions,
      });
    } catch (error) {
      return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
        message: 'Lỗi khi lấy danh sách hành động của nhân viên.',
      });
    }
  },

  getDetail: async (req, res) => {
    try {
      const employeeAction = await EmployeeActionModel.findById(req.params.id);
      if (!employeeAction) {
        return res.status(StatusCodes.NOT_FOUND).json({
          message: 'Không tìm thấy hành động của nhân viên với ID này.',
        });
      }
      return res.status(StatusCodes.OK).json({
        message: 'Lấy chi tiết hành động của nhân viên thành công.',
        data: employeeAction,
      });
    } catch (error) {
      return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
        message: 'Lỗi khi lấy chi tiết hành động của nhân viên.',
      });
    }
  },

  create: async (req, res) => {
    try {
      const { error } = employeeActionSchema.validate(req.body, {
        abortEarly: false,
      });
      if (error) {
        const errors = error.details.map((err) => err.message);
        return res.status(StatusCodes.BAD_REQUEST).json({
          message: 'Dữ liệu đầu vào không hợp lệ.',
          errors: errors,
        });
      }
      const employeeAction = await EmployeeActionModel.create(req.body);
      return res.status(StatusCodes.CREATED).json({
        message: 'Tạo hành động của nhân viên thành công.',
        data: employeeAction,
      });
    } catch (error) {
      return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
        message: 'Lỗi khi tạo hành động của nhân viên.',
      });
    }
  },

  edit: async (req, res) => {
    try {
      const { error } = employeeActionSchema.validate(req.body, {
        abortEarly: false,
      });
      if (error) {
        const errors = error.details.map((err) => err.message);
        return res.status(StatusCodes.BAD_REQUEST).json({
          message: 'Dữ liệu đầu vào không hợp lệ.',
          errors: errors,
        });
      }
      const employeeAction = await EmployeeActionModel.findByIdAndUpdate(
        req.params.id,
        req.body,
        { new: true },
      );
      if (!employeeAction) {
        return res.status(StatusCodes.NOT_FOUND).json({
          message: 'Không tìm thấy hành động của nhân viên với ID này.',
        });
      }
      return res.status(StatusCodes.OK).json({
        message: 'Cập nhật hành động của nhân viên thành công.',
        data: employeeAction,
      });
    } catch (error) {
      return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
        message: 'Lỗi khi cập nhật hành động của nhân viên.',
      });
    }
  },

  delete: async (req, res) => {
    try {
      const employeeAction = await EmployeeActionModel.findByIdAndDelete(
        req.params.id,
      );
      if (!employeeAction) {
        return res.status(StatusCodes.NOT_FOUND).json({
          message: 'Không tìm thấy hành động của nhân viên với ID này.',
        });
      }
      return res.status(StatusCodes.OK).json({
        message: 'Xóa hành động của nhân viên thành công.',
        data: employeeAction,
      });
    } catch (error) {
      return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
        message: 'Lỗi khi xóa hành động của nhân viên.',
      });
    }
  },
};

export default EmployeeActionController;
