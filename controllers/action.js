import ActionModel from '../model/action.js';
import { ActionSchema } from '../validate/action.js';

const ActionController = {
  getAll: async (req, res) => {
    try {
      const Actions = await ActionModel.find();
      return res.status(200).json({
        message: 'Lấy tất cả hành động của nhân viên thành công.',
        data: Actions,
      });
    } catch (error) {
      return res.status(500).json({
        message: 'Lỗi khi lấy danh sách hành động của nhân viên.',
      });
    }
  },

  getDetail: async (req, res) => {
    try {
      const Action = await ActionModel.findById(req.params.id);
      if (!Action) {
        return res.status(404).json({
          message: 'Không tìm thấy hành động của nhân viên với ID này.',
        });
      }
      return res.status(200).json({
        message: 'Lấy chi tiết hành động của nhân viên thành công.',
        data: Action,
      });
    } catch (error) {
      return res.status(500).json({
        message: 'Lỗi khi lấy chi tiết hành động của nhân viên.',
      });
    }
  },

  create: async (req, res) => {
    try {
      const action = ActionModel.findOne({ userId: req.user._id });
      const { error } = ActionSchema.validate(req.body, {
        abortEarly: false,
      });
      if (error) {
        const errors = error.details.map((err) => err.message);
        return res.status(400).json({
          message: 'Dữ liệu đầu vào không hợp lệ.',
          errors: errors,
        });
      }
      const Action = await ActionModel.create(req.body);
      return res.status(201).json({
        message: 'Tạo hành động của nhân viên thành công.',
        data: Action,
      });
    } catch (error) {
      return res.status(500).json({
        message: 'Lỗi khi tạo hành động của nhân viên.',
      });
    }
  },

  edit: async (req, res) => {
    try {
      const { error } = ActionSchema.validate(req.body, {
        abortEarly: false,
      });
      if (error) {
        const errors = error.details.map((err) => err.message);
        return res.status(400).json({
          message: 'Dữ liệu đầu vào không hợp lệ.',
          errors: errors,
        });
      }
      const Action = await ActionModel.findByIdAndUpdate(
        req.params.id,
        req.body,
        { new: true },
      );
      if (!Action) {
        return res.status(404).json({
          message: 'Không tìm thấy hành động của nhân viên với ID này.',
        });
      }
      return res.status(200).json({
        message: 'Cập nhật hành động của nhân viên thành công.',
        data: Action,
      });
    } catch (error) {
      return res.status(500).json({
        message: 'Lỗi khi cập nhật hành động của nhân viên.',
      });
    }
  },

  delete: async (req, res) => {
    try {
      const Action = await ActionModel.findByIdAndDelete(req.params.id);
      if (!Action) {
        return res.status(404).json({
          message: 'Không tìm thấy hành động của nhân viên với ID này.',
        });
      }
      return res.status(200).json({
        message: 'Xóa hành động của nhân viên thành công.',
        data: Action,
      });
    } catch (error) {
      return res.status(500).json({
        message: 'Lỗi khi xóa hành động của nhân viên.',
      });
    }
  },
};

export default ActionController;
