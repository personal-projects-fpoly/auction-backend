import ModelConversation from '../model/message.js';
import { io } from '../server.js';
import { schemaMessage } from '../validate/message.js';
export const MessageController = {
  getAll: async (req, res) => {
    try {
      const conversation = await ModelConversation.find().populate({
        path: 'userId',
      });
      if (!conversation) {
        return res
          .status(404)
          .json({ error: 'Không có cuộc trò chuyện tồn tại.' });
      }
      res.status(200).json(conversation);
    } catch (error) {
      res
        .status(500)
        .json({ error: 'Có lỗi xảy ra khi lấy thông tin cuộc trò chuyện.' });
    }
  },
  getById: async (req, res) => {
    try {
      const { id } = req.params;

      const conversation = await ModelConversation.findById(id).populate({
        path: 'userId',
      });
      if (!conversation) {
        return res
          .status(404)
          .json({ error: 'Cuộc trò chuyện không tồn tại.' });
      }

      res.status(200).json(conversation);
    } catch (error) {
      res
        .status(500)
        .json({ error: 'Có lỗi xảy ra khi lấy thông tin cuộc trò chuyện.' });
    }
  },
  getByUser: async (req, res) => {
    try {
      const { id } = req.params;
      const conversation = await ModelConversation.findOne({
        userId: id,
      }).populate({
        path: 'userId',
      });
      if (!conversation) {
        const data = await ModelConversation.create({ userId: id }).populate({
          path: 'userId',
        });
        return res.status(201).json(data);
      }

      res.status(200).json(conversation);
    } catch (error) {
      res
        .status(500)
        .json({ error: 'Có lỗi xảy ra khi lấy thông tin cuộc trò chuyện.' });
    }
  },
  async getByCategory(req, res) {
    try {
      const { category } = req.params;
      const messages = await ModelConversation.find({ category }).populate({
        path: 'userId',
      });

      res.status(200).json(messages);
    } catch (err) {
      res.status(500).json({ error: err.message });
    }
  },

  // Get messages by status
  async getByStatus(req, res) {
    try {
      const { status } = req.params;
      const messages = await ModelConversation.find({ status }).populate({
        path: 'userId',
      });

      res.status(200).json(messages);
    } catch (err) {
      res.status(500).json({ error: err.message });
    }
  },
  async getByStar(req, res) {
    try {
      const messages = await ModelConversation.find({ star: true });
      res.status(200).json(messages);
    } catch (err) {
      res.status(500).json({ error: err.message });
    }
  },

  // Get messages by label
  async getByLabel(req, res) {
    try {
      const { label } = req.params;
      const messages = await ModelConversation.find({ label }).populate({
        path: 'userId',
      });

      res.status(200).json(messages);
    } catch (err) {
      res.status(500).json({ error: err.message });
    }
  },

  addMessage: async (req, res) => {
    try {
      const { id } = req.params;
      const { sender, content, status } = req.body;

      const { value, error } = schemaMessage.validate({
        sender,
        content,
        status,
      });
      if (error) {
        const errorMessages = error.details.map((detail) => detail.message);
        return res.status(400).json({ error: errorMessages });
      }

      const conversation = await ModelConversation.findOne({ userId: id });
      if (!conversation) {
        return res
          .status(404)
          .json({ error: 'Cuộc trò chuyện không tồn tại.' });
      }
      const newMessage = {
        sender: value.sender,
        content: value.content,
        status: value.status,
        timestamp: new Date(),
      };
      conversation.messages.push(newMessage);
      conversation.save();
      io.emit(String(conversation._id), newMessage);
      res.status(200).json(newMessage);
    } catch (error) {
      res.status(500).json({
        error: 'Có lỗi xảy ra khi thêm tin nhắn vào cuộc trò chuyện.',
      });
    }
  },

  delete: async (req, res) => {
    try {
      const { id } = req.params;

      const conversation = await ModelConversation.findByIdAndDelete(id);
      if (!conversation) {
        return res
          .status(404)
          .json({ error: 'Cuộc trò chuyện không tồn tại.' });
      }

      res
        .status(200)
        .json({ message: 'Cuộc trò chuyện đã được xóa thành công.' });
    } catch (error) {
      res.status(500).json({ error: 'Có lỗi xảy ra khi xóa cuộc trò chuyện.' });
    }
  },
};
