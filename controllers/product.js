import { ProductModel } from '../model/product.js';
import { ProductValidationSchema } from '../validate/product.js';
import { io } from '../server.js';

export const ProductController = {
  getAll: async (req, res) => {
    try {
      const products = await ProductModel.find();
      res.status(200).json(products);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  },

  getOne: async (req, res) => {
    try {
      const productId = req.params.id;
      const product = await ProductModel.findById(productId);
      if (!product) {
        return res.status(404).json({ message: 'Product not found' });
      }
      res.status(200).json(product);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  },

  create: async (req, res) => {
    try {
      const { error } = ProductValidationSchema.validate(req.body);
      if (error) {
        return res.status(400).json({ message: error.details[0].message });
      }
      const newProduct = new ProductModel(req.body);
      const savedProduct = await newProduct.save();
      res.status(201).json(savedProduct);
    } catch (error) {
      res.status(400).json({ message: error.message });
    }
  },

  update: async (req, res) => {
    try {
      const productId = req.params.id;
      const updatedProduct = await ProductModel.findByIdAndUpdate(
        productId,
        req.body,
        { new: true },
      );
      if (!updatedProduct) {
        return res.status(404).json({ message: 'Product not found' });
      }
      res.status(200).json(updatedProduct);
    } catch (error) {
      res.status(400).json({ message: error.message });
    }
  },

  auction: async (req, res) => {
    try {
      const productId = req.params.id;
      // console.log(req.body);
      const { currentPrice, bids } = req.body;

      const updatedProduct = await ProductModel.findById(productId);

      if (!updatedProduct) {
        return res.status(404).json({ message: 'Product not found' });
      }
      if (currentPrice !== undefined) {
        if (currentPrice >= updatedProduct.currentPrice + updatedProduct.step) {
          updatedProduct.currentPrice = currentPrice;
        } else {
          return res.status(400).json({
            message: 'Bid must be higher than current price plus step',
          });
        }
      }
      if (bids && bids.length > 0) {
        updatedProduct.bids.unshift(...bids.reverse());
      }
      const savedProduct = await updatedProduct.save();

      io.emit('updateProduct', savedProduct);
      console.log('Update product:', savedProduct);

      res.status(200).json(savedProduct);
    } catch (error) {
      res.status(400).json({ message: error.message });
    }
  },

  delete: async (req, res) => {
    try {
      const productId = req.params.id;
      const deletedProduct = await ProductModel.findByIdAndDelete(productId);
      if (!deletedProduct) {
        return res.status(404).json({ message: 'Product not found' });
      }
      res.status(200).json({ message: 'Product deleted successfully' });
    } catch (error) {
      res.status(400).json({ message: error.message });
    }
  },
};
