import fs from "fs";
export const UploadController = {
  create: (req, res) => {
    console.log(req.files);
    res.json({ status: true, url: req.files[0].filename });
  },
  getBySlug: async (req, res) => {
    try {
      const img = req.params.slug;
      const imagedir = fs.readFileSync("./uploads/" + img);
      res.contentType("image/jpeg");
      res.send(imagedir);
    } catch (error) {
      console.log(error);
    }
  },
};
