import jwt from 'jsonwebtoken';
import { UserModel } from '../model/user.js';

const protectRoute = async (req, res, next) => {
  try {
    console.log('Cookies:', req.cookies);
    const token = req.cookies.jwt;
    if (!token) {
      return res.status(401).json({
        error: 'Unauthorized - Không cung cấp token hoặc tìm thấy token',
      });
    }
    jwt.verify(token, process.env.JWT_SECRET, async (err, decoded) => {
      if (err) {
        if (err.name === 'TokenExpiredError') {
          return res.status(401).json({
            error: 'Unauthorized - Token đã hết hạn',
          });
        } else if (err.name === 'JsonWebTokenError') {
          return res.status(401).json({
            error: 'Unauthorized - Token không hợp lệ',
          });
        } else {
          return res.status(401).json({
            error: 'Unauthorized - Token không hợp lệ',
          });
        }
      }
      const user = await UserModel.findById(decoded.userId).select('-password');
      if (!user) {
        return res.status(404).json({ error: 'Không thể tìm thấy user' });
      }
      req.user = user;
      next();
    });
  } catch (error) {
    console.log('Error in protectRoute middleware: ', error.message);
    res.status(500).json({ error: 'Internal server error protectRoute' });
  }
};

export default protectRoute;
