// src/validations/ActionValidation.js
import Joi from 'joi';

export const ActionSchema = Joi.object({
  label: Joi.string()
    .valid('create', 'read', 'update', 'delete')
    .required()
    .messages({
      'any.only':
        'Nhãn hành động không hợp lệ. Phải là một trong các giá trị: create, read, update, delete.',
      'any.required': 'Loại hành động là bắt buộc.',
    }),
  entity: Joi.string()
    .valid(
      'account',
      'product',
      'order',
      'category',
      'promotion',
      'voucher',
      'message',
    )
    .required()
    .messages({
      'any.only':
        'Thực thể không hợp lệ. Phải là một trong các giá trị: account, product, order, category, promotion, voucher, message.',
      'any.required': 'Thực thể là bắt buộc.',
    }),
  entityId: Joi.string().required().messages({
    'any.required': 'ID thực thể là bắt buộc.',
  }),
  description: Joi.string().optional().allow('').messages({
    'string.base': 'Mô tả phải là một chuỗi ký tự.',
  }),
  metadata: Joi.object()
    .optional()
    .pattern(Joi.string(), Joi.string())
    .messages({
      'object.base': 'Dữ liệu bổ sung phải là một đối tượng.',
      'object.pattern.base': 'Dữ liệu bổ sung có định dạng không hợp lệ.',
    }),
  date: Joi.date().default(Date.now).messages({
    'date.base': 'Ngày phải là một ngày hợp lệ.',
  }),
});
