import Joi from 'joi';

export const schemaMessage = Joi.object({
  content: Joi.string().required(),
  status: Joi.string().required(),
  sender: Joi.object({
    senderId: Joi.string().required(),
    name: Joi.string().required(),
    avatar: Joi.string().required(),
  }).required(),
  timestamp: Joi.date(),
});
