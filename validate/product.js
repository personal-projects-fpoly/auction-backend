import Joi from "joi";

export const ProductValidationSchema = Joi.object({
  name: Joi.string().required(),
  shortDescription: Joi.string(),
  description: Joi.string(),
  category: Joi.string().required(),
  thumbnail: Joi.string().required(),
  images: Joi.array().items(Joi.string()),
  startPrice: Joi.number().required(),
  startTime: Joi.date(),
  endTime: Joi.date(),
  step: Joi.number(),
});
