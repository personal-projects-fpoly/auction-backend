import express from 'express';
import { CartController } from '../controllers/cart.js';

export const routeCart = express.Router();
routeCart.get('/', CartController.getAll);
routeCart.get('/:userId', CartController.getByUser);
routeCart.put('/:userId/:itemId', CartController.updateCartItem);
routeCart.delete('/:userId/:itemId', CartController.removeFromCart);
routeCart.delete('/:userId', CartController.clearCart);
