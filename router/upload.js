import express from "express";
import { UploadController } from "../controllers/upload.js";
import { upload } from "../model/upload.js";

export const routerUpload = express.Router();

routerUpload.post("/", upload.any(), UploadController.create);
routerUpload.get("/images/:slug", UploadController.getBySlug);
