// src/routes/ActionRoutes.js
import express from 'express';
import ActionController from '../controllers/action.js';

export const routerAction = express.Router();

routerAction.get('/', ActionController.getAll);
routerAction.get('/:id', ActionController.getDetail);
routerAction.post('/', ActionController.create);
routerAction.put('/:id', ActionController.edit);
routerAction.delete('/:id', ActionController.delete);
