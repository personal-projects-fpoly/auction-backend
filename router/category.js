import express from "express";
import { CategoryController } from "../controllers/category.js";

export const routeCategory = express.Router();

routeCategory.get("/", CategoryController.getAll);
routeCategory.get("/:id", CategoryController.getOne);
routeCategory.post("/", CategoryController.create);
routeCategory.put("/:id", CategoryController.update);
routeCategory.delete("/:id", CategoryController.delete);
