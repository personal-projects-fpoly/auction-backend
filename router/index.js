import { routerAction } from './action.js';
import { routeAuth } from './auth.js';
import { routeCart } from './cart.js';
import { routeCategory } from './category.js';
import { routeMessage } from './message.js';
import { routePayment } from './payment.js';
import { routeProduct } from './product.js';
import { routerUpload } from './upload.js';

export function Route(app) {
  app.use('/product', routeProduct);
  app.use('/category', routeCategory);
  app.use('/auth', routeAuth);
  app.use('/action', routerAction);
  app.use('/upload', routerUpload);
  app.use('/message', routeMessage);
  app.use('/cart', routeCart);
  app.use('/payment', routePayment);
}
