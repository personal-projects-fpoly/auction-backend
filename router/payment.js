import express from 'express';
import { PaymentController } from '../controllers/payment.js';

export const routePayment = express.Router();
routePayment.get('/', PaymentController.getPayments);
// routePayment.get('/:userId', PaymentController.getByUser);
routePayment.post('/', PaymentController.createPayment);
routePayment.post('/qr', PaymentController.createQrCode);
// routePayment.put('/:userId/:itemId', PaymentController.updatePaymentItem);
// routePayment.delete('/:userId/:itemId', PaymentController.removeFromPayment);
// routePayment.delete('/:userId', PaymentController.clearPayment);
