import express from 'express';
import { MessageController } from '../controllers/message.js';

export const routeMessage = express.Router();

routeMessage.get('/', MessageController.getAll);
routeMessage.get('/:id', MessageController.getById);
routeMessage.get('/user/:id', MessageController.getByUser);
routeMessage.get('/label/:label', MessageController.getByLabel);
routeMessage.get('/category/:category', MessageController.getByCategory);
routeMessage.get('/status/:status', MessageController.getByStatus);
routeMessage.get('/star', MessageController.getByStar);
routeMessage.put('/:id', MessageController.addMessage);
routeMessage.delete('/:id', MessageController.delete);
