import express from 'express';
import { AuthController } from '../controllers/auth.js';
import protectRoute from '../middleware/protectedAuth.js';

export const routeAuth = express.Router();

routeAuth.get('/protected', protectRoute, AuthController.signinByToken);
routeAuth.post('/signin', AuthController.signin);
routeAuth.post('/signup', AuthController.signup);
routeAuth.post('/signout', AuthController.signout);
