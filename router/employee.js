// src/routes/employeeActionRoutes.js
import express from 'express';
import EmployeeActionController from '../controllers/employeeAction.js';

const routerEmployeeAction = express.Router();

routerEmployeeAction.get('/', EmployeeActionController.getAll);
routerEmployeeAction.get('/:id', EmployeeActionController.getDetail);
routerEmployeeAction.post('/', EmployeeActionController.create);
routerEmployeeAction.put('/:id', EmployeeActionController.edit);
routerEmployeeAction.delete('/:id', EmployeeActionController.delete);

export default routerEmployeeAction;
