import express from 'express';
import { ProductController } from '../controllers/product.js';

export const routeProduct = express.Router();

routeProduct.get('/', ProductController.getAll);
// routeProduct.get("/ed", ProductController.getAuctioned);
// routeProduct.get("/ing", ProductController.getAuctioning);
// routeProduct.get("/will", ProductController.getWillAuction);
routeProduct.get('/:id', ProductController.getOne);
// routeProduct.get("/ongoing", ProductController.getOngoing);
// routeProduct.get("/completed", ProductController.getCompleted);
routeProduct.post('/', ProductController.create);
routeProduct.put('/:id', ProductController.update);
routeProduct.put('/auction/:id', ProductController.auction);
routeProduct.delete('/:id', ProductController.delete);
