import cron from 'node-cron';
import { ProductModel } from '../model/product.js';
import { CartModel } from '../model/cart.js';
const handleAuctionEnd = async (auctionId, idBidder, quantity) => {
  try {
    const product = await ProductModel.findById(auctionId);
    if (!product) {
      console.error('Product not found.');
      return;
    }

    let cart = await CartModel.findOne({ userId: idBidder });

    if (!cart) {
      cart = new CartModel({ userId: idBidder, products: [] });
    }

    const cartItem = cart.products.find((item) =>
      item.productId.equals(auctionId),
    );

    if (cartItem) {
      cartItem.quantity += quantity;
      cartItem.totalPrice = (
        cartItem.quantity *
        (parseFloat(product.price) - parseFloat(cartItem.discount || 0))
      ).toFixed(2);
    } else {
      cart.products.push({
        productId: auctionId,
        price: product.currentPrice,
        quantity,
        totalPrice: (
          quantity *
          (parseFloat(product.currentPrice))
        ).toFixed(2),
      });
    }
    await cart.save();
  } catch (error) {
    console.error('Error occurred in handleAuctionEnd:', error);
  }
};

const checkEndedAuctions = async () => {
  try {
    const now = new Date();
    const endedAuctions = await ProductModel.find({
      endTime: { $lte: now },
      status: 'active',
    });

    if (!endedAuctions || endedAuctions.length === 0) {
      console.log('No auctions found');
      return;
    }

    for (const auction of endedAuctions) {
      const idBidder = auction.bids[0].user;
      await handleAuctionEnd(auction._id, idBidder, 1);

      auction.status = 'ended';
      await auction.save();
    }
  } catch (error) {
    console.error('Error occurred in checkEndedAuctions:', error);
  }
};

const auction = cron.schedule('* * * * *', async () => {
  console.log('Checking for ended auctions...');
  await checkEndedAuctions();
});

export default auction;
