const apiUrl =
  'https://script.google.com/macros/s/AKfycby0AuG-YuCvx1uW10Hwow9kGBZd0d9xKcj48-b2yiVEds9KqGubsRl2eVowB_YlT9P7cw/exec';

const callApi = async () => {
  try {
    const response = await fetch(apiUrl);
    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }
    const data = await response.json();
    if (data && Array.isArray(data.data)) {
      return data.data;
    } else {
      console.error(
        'Invalid data format: expected an object with a data array, but got:',
        data,
      );
      return null;
    }
  } catch (error) {
    console.error('Error occurred in callApi:', error);
    return null;
  }
};

const checkDescription = (payments, infoPayment) => {
  return payments.find((payment) => {
    console.log(payment);
    console.log(infoPayment);

    const datePayment = new Date(payment.transactionDate).getTime();
    const checkDescription = payment.description.includes(
      infoPayment.description,
    );
    return (
      checkDescription &&
      datePayment >= infoPayment.startTime &&
      datePayment <= infoPayment.startTime + 360000
    );
  });
};

const paymentApiCall = async (infoPayment) => {
  const startTime = Date.now();
  let check = true;
  let result = null;

  while (Date.now() - startTime < 120000 && check) {
    const data = await callApi();
    if (data && Array.isArray(data)) {
      const checkData = checkDescription(data, infoPayment);
      if (checkData) {
        console.log('Payment found:', checkData);
        check = false;
        result = checkData;
        break;
      }
    } else {
      console.error('Invalid data format:', typeof data);
    }

    await new Promise((resolve) => setTimeout(resolve, 1000));
  }

  return result;
};

export default paymentApiCall;
